package com.example.control_de_asistencia;

import android.app.Activity;
import android.content.Intent;
import android.widget.ListView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.example.control_de_asistencia.Adapter.SessionAdapter;
import com.example.control_de_asistencia.Model.Courses;
import com.example.control_de_asistencia.Model.Session;

import java.util.ArrayList;
import java.util.List;

public class SessionPage extends Activity {

    TextView subtitle;
    String courseTitle = "";
    List<Session> sessionList =  new ArrayList<>();
    ListView sessionDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session_page);
        subtitle = findViewById(R.id.subtitle);
        sessionDetails = findViewById(R.id.sessionDetail);

        populateSession();
        onNewIntent(getIntent());

        subtitle.setText(courseTitle);
        SessionAdapter sessionAdapter = new  SessionAdapter(this, sessionList);
        sessionDetails.setAdapter(sessionAdapter);

    }

    @Override
    protected void onNewIntent(Intent intent) {
       Bundle extras  = intent.getExtras();
       if(extras != null){
           if(extras.containsKey("COURSE")){
               courseTitle = extras.getString("COURSE");
           }
       }

    }

    private void populateSession(){
        Session session1 = new Session("Sesion I", "Finalizada", "14/09/17", "14:00  14:05","19:00  19:15");
        Session session2 = new Session("Sesion II", "Finalizada", "14/09/17", "14:00  14:05","19:00  19:15");
        Session session3 = new Session("Sesion III", "Finalizada", "14/09/17", "14:00  14:05","19:00  19:15");
        Session session4 = new Session("Sesion IV", "No Realizada", "14/09/17", "14:00  14:05","19:00  19:15");
        Session session5 = new Session("Sesion V", "Finalizada", "14/09/17", "14:00  14:05","19:00  19:15");
        Session session6 = new Session("Sesion VI", "Finalizada", "14/09/17", "14:00  14:05","19:00  19:15");
        Session session7 = new Session("Sesion VII", "Abierta", "14/09/17", "14:00  14:05","19:00  19:15");
        sessionList.add(session1);
        sessionList.add(session2);
        sessionList.add(session3);
        sessionList.add(session4);
        sessionList.add(session5);
        sessionList.add(session6);
        sessionList.add(session7);
    }
}
