package com.example.control_de_asistencia;

import android.app.Activity;
import android.widget.ListView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.example.control_de_asistencia.Adapter.CourseAdapter;
import com.example.control_de_asistencia.Model.Courses;

import java.util.ArrayList;
import java.util.List;

public class CoursesPage extends Activity {

    ListView coursesDetail;
    List<Courses> coursesList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_curses_page);

        coursesDetail = findViewById(R.id.coursesDetail);

        populateDetail();

        CourseAdapter courseAdapter = new CourseAdapter(this, coursesList);
        coursesDetail.setAdapter(courseAdapter);
    }


    public void populateDetail(){
        Courses courses1 = new Courses("Android: Modulo 1", "Entender los conocimientos basicos de programacion movil en Android Manifest, Activity, intent y listview");
        Courses courses2 = new Courses("Marketing digital: Modulo 1", "Entender los conocimientos basicos de programacion movil en Android Manifest, Activity, intent y listview");
        Courses courses3 = new Courses("Diseño web responsive: Modulo 1", "Entender los conocimientos basicos de programacion movil en Android Manifest, Activity, intent y listview");
        coursesList.add(courses1);
        coursesList.add(courses2);
        coursesList.add(courses3);
    }
}
