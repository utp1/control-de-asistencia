package com.example.control_de_asistencia;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class RegisterScreen extends Activity {

    private CheckBox conditionsCheck;

    private Button registerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_screen);

        conditionsCheck = findViewById(R.id.conditionsCheck);
        registerButton = findViewById(R.id.registerButton);

        conditionsCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                showTermsAndConditions();
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });

    }
    private void showTermsAndConditions(){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setTitle("Terminos y condiciones");
        alertDialog.setMessage("Todo texto, informacion, datos graficos, codigo fuente y codigo objeto, muestras de audio y video marcas y logotipos y similares pertenecen al desarrollador de esta aplicacion movil.");
        alertDialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener(){

            @Override
            public void onClick(DialogInterface dialog, int which) {
                     conditionsCheck.setChecked(true);
            }
        });

        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                conditionsCheck.setChecked(false);
            }
        });

        alertDialog.show();

    }

    private void register(){
        Intent intent = new Intent(this, DashboardScreen.class);
        startActivity(intent);
    }
}
