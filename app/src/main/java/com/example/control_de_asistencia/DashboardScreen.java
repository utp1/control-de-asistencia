package com.example.control_de_asistencia;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

public class DashboardScreen extends Activity {

    private LinearLayout cursosSection;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_screen);

        cursosSection = findViewById(R.id.cursosSection);

        cursosSection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cursos();
            }
        });
    }

    private void cursos(){
        Intent intent = new Intent(this, CoursesPage.class);
        startActivity(intent);
    }
}
