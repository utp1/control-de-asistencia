package com.example.control_de_asistencia.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.example.control_de_asistencia.Model.Courses;
import com.example.control_de_asistencia.Model.Session;
import com.example.control_de_asistencia.R;
import com.example.control_de_asistencia.SessionPage;
import org.w3c.dom.Text;

import java.util.List;

public class CourseAdapter extends ArrayAdapter<Courses> {

    Context context;
    List<Courses> courses;

    public CourseAdapter( Context context, List<Courses> courses) {
        super(context, R.layout.activity_modules_list_page, courses);
        this.context = context;
        this.courses = courses;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View  item = convertView;
        if (item ==null)
          item = LayoutInflater.from(context).inflate(R.layout.activity_modules_list_page, null);

        TextView title = (TextView) item.findViewById(R.id.title);
        TextView subtitle = (TextView) item.findViewById(R.id.subtitle);

        title.setText(this.courses.get(position).getTitle());
        subtitle.setText(this.courses.get(position).getSubtitle());
        item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SessionPage.class);
                intent.putExtra("COURSE", courses.get(position).getTitle());
                context.startActivity(intent);
            }
        });
        return item;
    }
}
