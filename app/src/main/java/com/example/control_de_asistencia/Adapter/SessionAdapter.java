package com.example.control_de_asistencia.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.example.control_de_asistencia.Model.Session;
import com.example.control_de_asistencia.R;

import java.util.List;

public class SessionAdapter extends ArrayAdapter<Session> {

    Context context;
    List<Session> sessions;

    public SessionAdapter(Context context, List<Session> sessions){
        super(context, R.layout.session_list, sessions);
        this.context = context;
        this.sessions = sessions;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View item = convertView;

        if(item == null){
            item = LayoutInflater.from(context).inflate(R.layout.session_list,null);

        }

        TextView sessionName = item.findViewById(R.id.sessionName);
        TextView sessionState = item.findViewById(R.id.sessionState);
        TextView sessionDate = item.findViewById(R.id.sessionDate);
        TextView sessionStart = item.findViewById(R.id.sessionStart);
        TextView sessionEnd = item.findViewById(R.id.sessionEnd);

        sessionName.setText(this.sessions.get(position).getSessionName());
        sessionState.setText(this.sessions.get(position).getSessionState());
        sessionDate.setText(this.sessions.get(position).getSessionDate());
        sessionStart.setText(this.sessions.get(position).getSessionStart());
        sessionEnd.setText(this.sessions.get(position).getSessionEnd());

        return item;
    }
}
