package com.example.control_de_asistencia.Model;

public class Courses {

    private String title;

    private String subtitle;

    public Courses(String title, String subtitle) {
        this.title = title;
        this.subtitle = subtitle;
    }

    public Courses() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }
}
