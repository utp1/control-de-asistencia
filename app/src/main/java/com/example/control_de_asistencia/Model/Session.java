package com.example.control_de_asistencia.Model;

import java.sql.Time;
import java.util.Date;
import java.util.Timer;

public class Session {

    private String sessionName;

    private String sessionState;

    private String sessionDate;

    private String sessionStart;

    private String sessionEnd;

    public Session(String sessionName, String sessionState, String sessionDate, String sessionStart, String sessionEnd) {
        this.sessionName = sessionName;
        this.sessionState = sessionState;
        this.sessionDate = sessionDate;
        this.sessionStart = sessionStart;
        this.sessionEnd = sessionEnd;
    }

    public String getSessionName() {
        return sessionName;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName;
    }

    public String getSessionState() {
        return sessionState;
    }

    public void setSessionState(String sessionState) {
        this.sessionState = sessionState;
    }

    public String getSessionDate() {
        return sessionDate;
    }

    public void setSessionDate(String sessionDate) {
        this.sessionDate = sessionDate;
    }

    public String getSessionStart() {
        return sessionStart;
    }

    public void setSessionStart(String sessionStart) {
        this.sessionStart = sessionStart;
    }

    public String getSessionEnd() {
        return sessionEnd;
    }

    public void setSessionEnd(String sessionEnd) {
        this.sessionEnd = sessionEnd;
    }
}
