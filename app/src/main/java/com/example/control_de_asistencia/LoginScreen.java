package com.example.control_de_asistencia;

import android.app.Activity;
import android.content.Intent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class LoginScreen extends Activity {

    private Button loginButton;
    private TextView forgotText;
    private Button registerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);

         loginButton = findViewById(R.id.loginButton);
         forgotText = findViewById(R.id.forgotText);
         registerButton = findViewById(R.id.registerButton);


        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

        forgotText.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                forgotPassword();
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });
    }

    public void login(){
        Intent intent = new Intent(this, DashboardScreen.class);
        this.startActivity(intent);
        finish();
    }

    public void forgotPassword(){
        Intent intent = new Intent(this, RecoverPassword.class);
        this.startActivity(intent);
    }

    public void register(){
        Intent intent = new Intent(this, RegisterScreen.class);
        this.startActivity(intent);
    }

}
